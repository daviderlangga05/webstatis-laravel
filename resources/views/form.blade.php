<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Form</title>
</head>
<h1>Buat Account Baru!</h1>
<h4>Sign Up Form</h4>
<form action="{{url ('/welcome')}}" method="get">

  <p>First name:</p>

  <input type="text" id="fname" name="fname">

  <p>Last name:</p>

  <input type="text" id="lname" name="lname">

  <p>Gender:</p>
  <input type="radio" name="gender" value="0">Male</input>
  <br>
  <input type="radio" name="gender" value="1">Female</input>
  <br>
  <input type="radio" name="gender" value="2">Other</input>

  <p>Nationality:</p>

  <select>
    <option value="indonesia">Indonesia</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Singapore">Singapore</option>
  </select>

  <p>Language Spoken:</p>
  <input type="checkbox" name="language" value="1">Bahasa Indonesia</input>
  <br>
  <input type="checkbox" name="language" value="1">English</input>
  <br>
  <input type="checkbox" name="language" value="1">Other</input>

  <p>Bio:</p>
  <textarea cols="30" rows="7"> </textarea>
  <br>
  <input type="submit" value="Sign Up">

</form>


</body>

</html>