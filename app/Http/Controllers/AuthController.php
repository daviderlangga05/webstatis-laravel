<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function form()
    {
        return view('form');
    }

    function welcome(Request $request)
    {
        $first = $request->fname;
        $last = $request->lname;
        // dd($first, $last);
        // return view('welcome');
        return view('welcome', compact('first', 'last'));
    }
}
